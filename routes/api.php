<?php

use App\Http\Controllers\UsersApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Nekogas e bitno koga ruta ke bide prva, pa vtora itn
Route::get('users/search', [UsersApiController::class, 'search'])->middleware('auth:api');
// Route::get('/users/list', [UsersApiController::class, 'index']);
Route::apiResource('users', UsersApiController::class)->middleware('auth:api');


