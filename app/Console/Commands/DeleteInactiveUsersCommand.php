<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class DeleteInactiveUsersCommand extends Command
{
    protected $signature = 'brainster:delete-deleted-users';


    protected $description = 'Brisenje na neaktivni useri';

    public function handle()
    {
       // Site izbrishani (softDelete) koi se neaktivni poveke od 6 meseci
       $inactive = User::onlyTrashed()->where('deleted_at', '<', now()->subMonths(6))->get();

       $count = 0;
       foreach($inactive as $inactive_user){
           $inactive_user -> forceDelete();
           $count++;
       }
       $this->info('So komandava izbrishavme {$count} user');
    }
}
