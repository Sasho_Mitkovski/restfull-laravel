<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UsersApiController extends Controller
{

    public function index()
    {
        $users = User::orderByDesc('id')->get();
        // $users = User::all();
        return response()->json($users);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',

        ]);

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->api_token = Str::random(80);
        $user->save();

        // Gi vrakame bidejki ke ni gi treba da gi stavime vo listata
        return response()->json($user);
        // dd($request->all());
    }

    public function show($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        return response()->json($user);
    }

    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        $q = $request->input('q');
        // $users = User::where('name', 'LIKE', "%{$q}%")->orWhere('email', 'LIKE', "%{$q}%")->get();

        $users = User::whereNested(function ($query) use ($q) {
            $query->where('name', 'LIKE', "%{$q}%")->orWhere('email', 'LIKE', "%{$q}%");
        })->get();
        return response()->json($users);
    }
}
