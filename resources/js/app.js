require("./bootstrap");
var token = $('meta[name="api-token"]').attr('content')
var headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + token
}

// Prikaz na forma so klik
$("#btn-new-user").on("click", function (event) {
    event.preventDefault();
    $("#users-new").slideToggle();
});
var token = $('meta[name="api-token"]').attr('content')
// Prikaz ( print ) na site users na homepage
fetch("/api/users/", {
    method: "GET",
    headers: headers
})
    .then((response) => response.json())
    .then((data) => {
        console.log("Success:", data);
        $.each(data, function (key, user) {
            $("#users-list tbody").append(`
                <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.email}</td>
                <td><button type="button" class="btn btn-info edit-user-btn" data-user-id="${user.id }"/> Edit</button></td>

                  </tr>
          `);
        });
    })
    .catch((error) => {
        console.error("Error:", error);
    });
//============================================================================================
// Kreiranje na new-User
$("#users-new").on("submit", function (event) {
    event.preventDefault();

    // console.log($(this).attr('action'))
    var action = $(this).attr("action"); // Ova e linkot- po attribut action od formata
    fetch(action, {
        method: "POST",
        headers: headers,
        body: JSON.stringify({
            // zemanje na novite vrednosti od input polinjata
            name: $(this).find('input[name = "name"]').val(),
            email: $(this).find('input[name = "email"]').val(),
            password: $(this).find('input[name = "password"]').val(),
        }),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log("Success:", data);
            // Kod za pokazuvanje na errors======================
            if (data.errors) {
                var poraka = " ";
                $.each(data.errors, function (fieldName, fieldErrorArray) {
                    poraka = poraka + `${fieldName} errors:` + fieldErrorArray;
                });
                alert(poraka); //Ako e najdena greska, pokazi
            } else {
                // AKo e ok, prontaj nov red za userot
                $("#users-list tbody").prepend(`
                    <tr>
                    <td>${user.id}</td>
                    <td>${user.name}</td>
                    <td>${user.email}</td>
                      </tr>
              `);
            }
        })
        .catch((error) => {
            console.error("Error:", error);
        });
});
// Update na user ====================================================================================

$("#users-list").on("click", ".edit-user-btn", function (event) {
    var user_id = $(this).attr('data-user-id');// ova ni e ID na userot na koj sme kliknale
    // Link za da gi zemame starite informacii vo forma
    $.ajax({
        type: "GET",
        url: '/api/users/' + user_id,
        contentType: "application/json",
        headers: headers,
    })
     // Zemanje na starite podatoci
        .done(function (data) {
           $('#users-update input[name="id"]').val(data.id);
           $('#users-update input[name="name"]').val(data.name);
           $('#users-update input[name="email"]').val(data.email);
        })
        .fail(function (msg) {
            console.log("FAIL", msg);
        })

    $("#users-update").slideToggle();
});
// ===========  Zapishuvanje na novite vrednosti vo baza  =========================================
$("#users-update").on("submit", function (event) {
    event.preventDefault();
// Ova e linkot- po attribut action od formata + ID od poleto id
    var action = $(this).attr("action") + "/" + $(this).find('input[name = "id"]').val();

    $.ajax({
        type: "PUT",
        url: action,
        contentType: "application/json",
        headers: headers,
        data: JSON.stringify({
            name: $(this).find('input[name = "name"]').val(),
            email: $(this).find('input[name = "email"]').val(),
        }),
    })
        .done(function (data) {
            console.log("SUCCESS", data);
        })
        .fail(function (msg) {
            console.log("FAIL", msg);
        })
        .always(function (msg) {
            console.log("ALWAYS", msg);
        });
        $("#users-update").hide();
});
