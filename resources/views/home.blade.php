@extends('layouts.app')
@section('title', 'Tabela')
@section('content')
<div class="container">
    <div class="d-flex justify-content-end">
    <button id="btn-new-user" class="btn btn-success my-2 " type="submit">+ New User</button>
    </div>

    <form class="form col-md-6 offset-6 " id="users-new" action="{{ route('users.store') }}" method="POST" style="display: none">
            <input type="text" name="name" class="form-control mt-3" placeholder="Name"/>
            <input type="text" name="email" class="form-control mt-3" placeholder="Email"/>
            <input type="password" name="password" class="form-control mt-3"  placeholder="Password"/>
            <button class="btn btn-primary my-3 w-100" type="submit">Create User</button>
    </form>
    {{--  Update forma  --}}
    <form class="form col-md-6 offset-6 " id="users-update" action="{{ route('users.store') }}" method="PUT" style="display: none">
            <input type="hidden" name="id" value=""/>
            <input type="text" name="name" class="form-control mt-3" placeholder="Name"/>
            <input type="text" name="email" class="form-control mt-3" placeholder="Email"/>

            <button class="btn btn-primary my-3 w-100" type="submit">Update User</button>
    </form>
<table id="users-list" class="table">
    <thead>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
        <th>Action</th>
    </thead>
    <tbody>
{{-- Tuka so JavaScript dodavame koloni --}}
    </tbody>
</table>
</div>
@endsection
