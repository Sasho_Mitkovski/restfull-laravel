<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AddApiTokenUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->string('api_token', 80);
            });
          $users = User::all();
          foreach($users as $user){
              $user->api_token = Str::random(80);
              $user->save();
          }

    }
    
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->dropColumn(['api_token']);
        });

    }

}
